﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
   // siia ma võin ka kirjutada klasse, enumme ja structe

    // klasside, struktide, enummide JÄRJEKORD namespaces ei ole oluline

    interface ISöödav
    {
        void Söö();
    }

    class Sepik : ISöödav
    {
        public void Söö()
        {
            Console.WriteLine("Keegi nosib sepikut"); 
        }
    }


    class Loom : IComparable
    {
        public static List<Loom> Loomaaed = new List<Loom>();
        
        public readonly string Liik;        // looma liiki peale "loomist" muuta ei saa
        //public Loom() { }
        public Loom(string liik)        // ilma liigita looma ei saa teha 
        {
            Liik = liik;
            Loomaaed.Add(this);
        }

        // vistual tähendab, et tuletatud klassis on võimalik see meetod ära muuta
        // ümber defineerid ehk overraidida
        public virtual void TeeHäält() 
        { 
            Console.WriteLine($"{Liik} teeb koledat häält"); 
        }

        public override string ToString() => $"loom liigist {Liik}";

        public virtual void Pigista() { }

        int IComparable.CompareTo(object obj)
        {
            if (obj is Loom l) return this.Liik.CompareTo(l.Liik);
            else return -1;
        }
    }
    class Koduloom : Loom
    {
        public string Nimi;
        public Koduloom(string nimi, string liik) : base(liik)
        {
            // Kodulooma tegemisel PEAB enne Looma tegema ja talle liigi ütlema 
            // base(liik) on pöördumine baasklassi (Loom) konstruktori poole
            Nimi = nimi;
        }
        public override string ToString() => $"{Liik} {Nimi}"; // seda overridi vaatame hiljem

        public override void TeeHäält() => Console.WriteLine($"väike {Nimi} möriseb mahedasti");

        public override void Pigista()
        {
            throw new NotImplementedException();
        }
    }

    class Kass : Koduloom
    {
        public string Tõug;
        private bool Tuju = false;

        public void SikutaSabast() => Tuju = false;
        public void Silita() => Tuju = true;

        public Kass() : base("nimeta", "kass") { }

        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kass {Nimi} lööb nurru");
            else Console.WriteLine($"{Nimi} kräunub");
        }


    }

    class Koer : Koduloom , ISöödav
    {
        public string KoeraTõug; // see ei ole sama asi, mis kassi tõug
        public Koer() : base("nimeta", "koer") { }

        public void Söö()
        {
            Console.WriteLine($"{Nimi} pannakse nahka"); 
        }

        public override void TeeHäält()
        {
            Console.WriteLine($"{Nimi} haugub usinasti");
        }
    }

}
