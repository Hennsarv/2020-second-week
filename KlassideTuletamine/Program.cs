﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KlassideTuletamine
{
    static class Program
    {
        static void Main(string[] args)
        {
            // Main-i ma kasutan testimiseks ja näitamiseks

            Loom loom1 = new Loom("krokodill");
            //Console.WriteLine(loom1);

            Loom loom2 = new Loom("ämblik");
            //Console.WriteLine(loom2);

            Koduloom koduloom1 = new Koduloom("Albert", "prussakas");
            //Console.WriteLine(koduloom1);

            Kass kiisu = new Kass() { Nimi = "Carfield", Tõug = "kaarfiildi tõug" };
            //Console.WriteLine(kiisu);

            kiisu.Silita();

            foreach (var l in Loom.Loomaaed)
            {
                if(l is ISöödav ls) { ls.Söö(); } 
                else l.TeeHäält();
            }

            Koer k = new Koer { Nimi = "Polla" };
            //k.TeeHäält();

            Sepik s = new Sepik();
            s.Söö();
            k.Söö();

            Lõuna(k);

            var loomad = Loom.Loomaaed.ToArray();

            Array.Sort(loomad);

            foreach(var xx in Loom.Loomaaed)
            {
                // ((Kass)xx).Silita(); // casting annab vea, kui xx ei ole Kass
                (xx as Kass)?.Silita(); // as-operaator annab null kui xx ei ole Kass
                if (xx is Kass xxs) xxs.Silita();
            }
        }

        public static  void Lõuna(object x)
        {
            if (x is ISöödav xs) xs.Söö();
            else Console.WriteLine("täna jääme nälga");
        }
    }

    // siia structid, enumid ja klassid - siia täna ei pane midagi

}
