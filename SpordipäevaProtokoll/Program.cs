﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipäevaProtokoll
{
    class Program
    {
        static string Küsi(string küsimus)  // see on funktsioon
        {
            Console.Write(küsimus + ": ");
            string vastus = Console.ReadLine();
            if (vastus == "") return "ta ei taha öelda";
            else return vastus;
        }

        static void Trüki(string nimi, string sisu) // see on meetod
        {
            Console.WriteLine($"{nimi}={sisu}");
        }

        static void Main(string[] args)
        {
            //Console.Write("Kes sa oled: ");
            //string nimi = Console.ReadLine();

            //Console.WriteLine("Kus sa elad: ");
            //string aadress = Console.ReadLine();

            string nimi = Küsi("Kes sa oled");
            string aadress = Küsi("Kus sa elad");

            Trüki("tema nimi on: ", nimi);
            Trüki("tema aadress on: ", aadress);


            Trüki("kontrollküsimus", Küsi("kus ta elab") + Küsi("mis ta nimi on"));




            string fileName = @"..\..\spordipäeva protokoll.txt";
            var loetudRead = System.IO.File.ReadAllLines(fileName);
            List<ProtokolliRida> protokoll = new List<ProtokolliRida>();

            for (int i = 1; i < loetudRead.Length; i++)
            {
                var tükid = loetudRead[i].Replace(", ","," ).Split(',');
                if (tükid.Length < 3) break;
                var rida = new ProtokolliRida
                {
                    Nimi = tükid[0],
                    Distants = int.Parse(tükid[1]),
                    Aeg = int.Parse(tükid[2]),
                };
                if (rida.Aeg == 0) break;
                
                protokoll.Add(rida);
            }

            foreach (var x in protokoll) Console.WriteLine(x);



        }
    }

    class ProtokolliRida
    {
        public string Nimi;
        public int Distants;
        public int Aeg;

        // näide meetodist
        public void TrükiTulemus() // meetod
        {
            Console.WriteLine($"{Nimi} jooksis {Distants} meetrit ajaga {Kiirus()}");
        }

        // näide funktsioonist
        public double Kiirus() // funktsioon
        {
            return Distants * 1.0 / Aeg;
        }


        // see on ka funktsioon
        public override string ToString()
        {
            return $"{Nimi} jooksis {Distants}m Ajaga {Aeg}";
        }

    }


}
