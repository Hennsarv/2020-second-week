﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LihtneHarjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            new Inimene("35503070211") { Eesnimi = "henn", Perenimi = "SARV" };
            new Inimene("45705210211") { Eesnimi = "MAris", Perenimi = "SARV" };
            new Inimene("61601260000") { Eesnimi = "grete", Perenimi = "ValdVee" };
            new Inimene("35503070211") { Eesnimi = "henn", Perenimi = "SARV" };
            */
            var i = Inimene.New("35503070211");  // isikukoodi ei ole - tehakse uus
            i.Eesnimi = "henn";
            i.Perenimi = "sarv";

            {
                if (Inimene.TeeInimene("35503070211", out Inimene uus))
                {
                    uus.Eesnimi = "Henn";
                    uus.Perenimi = "sarv";
                }
            }
            {
                if (Inimene.TeeInimene("45705050211", out Inimene uus))
                {
                    uus.Eesnimi = "Henn";
                    uus.Perenimi = "sarv";
                }
            }
            {
                if (Inimene.TeeInimene("35503070211", out Inimene uus))
                {
                    uus.Eesnimi = "Henn";
                    uus.Perenimi = "sarv";
                }
            }



            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);
            Console.WriteLine(Inimene.InimesteArv);
        }
    }
}
