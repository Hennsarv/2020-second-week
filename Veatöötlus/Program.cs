﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Veatöötlus
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Minu väike sünnipäevanäide
            //Console.Write("sünnipäev: ");
            //string vastus = Console.ReadLine();
            //DateTime sp =
            //    DateTime.TryParse(vastus, new CultureInfo("et-ee"), DateTimeStyles.AllowInnerWhite, out DateTime dte) ? dte :
            //    DateTime.TryParse(vastus, new CultureInfo("fi-fi"), DateTimeStyles.AllowInnerWhite, out DateTime dts) ? dts :
            //    DateTime.TryParse(vastus, new CultureInfo("fr-fr"), DateTimeStyles.AllowInnerWhite, out DateTime dtf) ? dtf :
            //    DateTime.Today;
            //Console.WriteLine(sp); 
            #endregion

            try
            {
                Console.Write("Anna üks arv: ");
                int arv = int.Parse(Console.ReadLine());
                Console.Write("Anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());
                if (teine != 0)
                    Console.WriteLine(arv / teine);
                else
                {
                    Console.WriteLine("nulliga mina ei jaga");
                    throw new Exception("ise oled null");
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine($"midagi juhtus ({e.Message}), ma täna ei jaga");
                throw;
            }
            finally
            {
                Console.WriteLine("pulmalaud tuleb ikka kinni maksta");
            }

            Console.WriteLine("kõik hästi");

            /*
             * try  {}     - proovi
             * catch {}   - püüa kinni õnnetus
             * finally {}  - pulmalaua teema
             * throw ;   - helista tuletõrjesse
             */
        }
    }
}
