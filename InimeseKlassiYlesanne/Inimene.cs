﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeseKlassiYlesanne
{
    class Inimene
    {
        public string Eesnimi { get; set; } // default property


        string _Perenimi; // käsitsi kirjutatud default propertyga samaväärne
        public string Perenimi 
        {
            get { return _Perenimi; }
            set { _Perenimi = value; }
        }
        
        private string _Isikukood = "";

        // setMeetod ja getFunktsioon võimaldavad kontrollitult suhelda privaatväljaga
        public void setIsikukood(string uusisikukood) // setter
        => _Isikukood = _Isikukood == "" ? uusisikukood : _Isikukood;
        public string getIsikukood() => _Isikukood; // getter 

        // see siin on PEAAEGU sama asi, aga kannab nime property
        public string Isikukood
        {
            get => _Isikukood;
            set => _Isikukood = _Isikukood == "" ? value : _Isikukood;
        }


        // siin on tegemist funktsiooniga, mis arvutab nime (kokkupandud)
        public string Nimi() => $"{Eesnimi} {Perenimi}";
        //{
        //    //return $"{Eesnimi} {Perenimi}";
        //    return Eesnimi + " " + Perenimi;
        //}

        // funktsiooni kirjutamine lühemalt
        // public string Nimi2() => Eesnimi + " " + Perenimi;

//        public string Nimi3 => Eesnimi + " " + Perenimi;

        public string Täisnimi => Eesnimi + " " + Perenimi; 
        


        // siit edasi ära vaata - nii ei tehta tegelikult
        public DateTime Sünniaeg
        => new DateTime(
                ((Isikukood[0] - '1') / 2) * 100 + 1800 +
                int.Parse(Isikukood.Substring(1, 2)),
                int.Parse(Isikukood.Substring(3, 2)),
                int.Parse(Isikukood.Substring(5, 2))
                );
        

        public DateTime Sünniaeg2()
        {
            return new DateTime(
                (Isikukood[0] == '1' ? 1800 :
                Isikukood[0] == '2' ? 1800 :
                Isikukood[0] == '3' ? 1900 :
                Isikukood[0] == '4' ? 1900 :
                Isikukood[0] == '5' ? 2000 :
                Isikukood[0] == '6' ? 2000 : 0) +
                int.Parse(Isikukood.Substring(1, 2)),
                int.Parse(Isikukood.Substring(3, 2)),
                int.Parse(Isikukood.Substring(5, 2))

                );

        }

        public DateTime Sünniaeg3()
        {
            switch(Isikukood[0])
            {
                case '1':
                case '2':
                    return new DateTime(1800 +
                int.Parse(Isikukood.Substring(1, 2)),
                int.Parse(Isikukood.Substring(3, 2)),
                int.Parse(Isikukood.Substring(5, 2))
                        );
                case '3':
                case '4':
                    return new DateTime(1900 +
                int.Parse(Isikukood.Substring(1, 2)),
                int.Parse(Isikukood.Substring(3, 2)),
                int.Parse(Isikukood.Substring(5, 2))
                        );
                case '5':
                case '6':
                    return new DateTime(2000 +
                int.Parse(Isikukood.Substring(1, 2)),
                int.Parse(Isikukood.Substring(3, 2)),
                int.Parse(Isikukood.Substring(5, 2))
                        );
            }
            return DateTime.Today;
        }



        public int Sugu => Isikukood[0] % 2;  // 0-naine, 1-mees

        public int Vanus
        => (DateTime.Today - Sünniaeg).Days * 4 / 1461;
        
    }
}
