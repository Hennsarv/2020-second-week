﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KoolipereÜlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            string õpilaneFile = @"..\..\Õpilased.txt";
            string õpetajaFile = @"..\..\Õpetajad.txt";
            string hindedFile = @"..\..\Hinded.txt";
            string ainedFile = @"..\..\KlassJaAine.txt"; // ei tea, kas läks vaja - Henn ütles, et ehk ei lähe
            // alustame õpilastega - IK, Nimi, klass
            var õpilased = File.ReadAllLines(õpilaneFile)
                .Select(x => x.Replace(", ", ",").Split(',')) // loetud read massiiviks
                .Select(x => new { Tüüp="õpilane", IK = x[0], Nimi = x[1], Klass = x[2], Aine = "" })
                .ToList() // salvestame listina, hea hiljem võtta
                ;
            // kontrolllime, kas toimib (unit test :))
            //foreach (var õ in õpilased) Console.WriteLine(õ); // ära kasuta õ-d muutuja nimes :)
            // sama asja võiks teha õpsidega
            var õppetajad = File.ReadAllLines(õpetajaFile)
                .Select(x => (x+",").Replace(", ", ",").Split(',')) // loetud read massiiviks
                .Select(x => new { Tüüp = "õpetaja", IK = x[0], Nimi = x[1], Klass = x[3], Aine = x[2] })
                .ToList() // salvestame listina, hea hiljem võtta
                ;
            //foreach (var õ in õppetajad) Console.WriteLine(õ);
            // siin kohal tekkis mul mõte!!! koolipere jaoks oleks hea, kui saaks kokku panna
            // selleks oleks vahva, kui mõlemad listid oleks ühesugused
            // teeme sellise kavaluse, et ka õpilaste listis on Aine aga see == ""
            var koolipere = õpilased.Union(õppetajad).ToDictionary(x => x.IK);
                //.ToList()   // seda on vaja, kui ma tahan kaks list KOOS mällu pista
                            // ilma selleta saab, siis loetaksegi iga kord kaks listi läbi
                ;
            // oot aga jube vahva oleks, kui mul kohe miski tunnus, kas õpilane või õpetaja
            foreach (var õ in koolipere) Console.WriteLine(õ);
            // väike tähelepanek, struktil on oma mugav ToString() juba olemas, klassil tuleb see teha

            // nüüd hinded
            var hinded          // ma alustan alati var-iga, kuna ma ei tea, mis "välja tuleb"
                = File.ReadAllLines(hindedFile)
                .Select(x => x.Split(','))
                .Select(x => new { ÕpsIK = x[0], ÕpilaneIK = x[1], Aine = x[2], Hinne = int.Parse(x[3]) })
                .ToLookup(x => x.ÕpilaneIK); // ahhaa - siis ma saan õpilase hinded kätte
            //if (false) // vahva viis järgmine lause alles jätta ilma välja kommenteerimata
            foreach(var õ in hinded)
            {
                Console.WriteLine($"õpilase {õ.Key} hinded:");
                foreach(var h in õ)
                    Console.WriteLine($"\t{h.Aine} {h.Hinne} pani õpetaja {koolipere[h.ÕpsIK].Nimi }"); // oo ägge, toimibki
            }

            // proovin nüüd leida ika õpilase keskmise hinde
            // õpilased on mul oma listis hinded on mul "dictionary" moodi asjas - võtmeks isikukood
            // st õpilase hinded saan am tema isikukoodi järgi

            foreach(
                var õ in õpilased
                .Select(x => new {x.Nimi, x.Klass,
                    Keskmine = hinded[x.IK]
                    .DefaultIfEmpty() // et ei tekiks keskmise arvutamisel peaks tühja nimekirja vastu hakkama
                    .Average(y => y?.Hinne??0)} // aga nüüd ei saa Hinnet lugeda, kuna y võib olla null
                                            // y?.Hinne - defaultifempty paneb nulli ja sellel hinnet ei ole
                                            // ??0 paneb sellistele tüüpidele "olematu" hinde keskmiseks
                    )
                .OrderByDescending(x => x.Keskmine)
                .ToLookup(x => x.Klass)
                .Select(x => new {Klass = x.Key, Parim = x.OrderByDescending(y => y.Keskmine).First()})
                ) Console.WriteLine(õ);

            // kõik on hea, kuna KÕIGIL õpilastel ON hindeid, proovime, mis juhtub, kui mõnel ei ole

            // proovin mõne aja pärast ka seda sünnipäevade asja, aga siis koos ja kõva häälega
            // 

            // oli vaja leida, kellel on järgmine nädal sünnipäev?
            // kuidas seda teha?

            // koolipere - seal on list, kus nimed ja isikukoodid
            var sünnipäevad = koolipere.Values
                .Select(x => new { Nimi = x.Nimi, Kuu = x.IK.Substring(3, 2), Päev = x.IK.Substring(5, 2) })
                .Select(x => new { x.Nimi, Sünnipäev = new DateTime(DateTime.Today.Year, int.Parse(x.Kuu), int.Parse(x.Päev)) })
                .Select(x => new { x.Nimi, Sünnipäev = x.Sünnipäev < DateTime.Today ? x.Sünnipäev.AddYears(1) : x.Sünnipäev })
                .ToList();
            Console.WriteLine("\nsünnipäevad\n");

            // lihtsaid asju ei suuda reedel enam kirja panna
            // vaata - see valem toimib
            DateTime uusNädal = DateTime.Today.AddDays(7 - ((int)DateTime.Now.DayOfWeek+6) % 7);

            Console.WriteLine($"uus nädal algab {uusNädal} ja lõpeb enne {uusNädal.AddDays(7)}");

            foreach (var s in sünnipäevad
                .OrderBy(x => x.Sünnipäev)
                .Take(5) // anna viis järgmist sünnipäeva
                //.Where(x => x.Sünnipäev >= uusNädal && x.Sünnipäev < uusNädal.AddDays(7))
                // annab järgmise nädala sünnipäevad
                ) Console.WriteLine(s);


            
        }


    }
}
