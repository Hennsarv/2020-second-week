﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IlmajaamaLogi
{
    static class Program
    {
        public static string LeiaTemp(this string katsetus)
        {
            try
            {
                string temp =
                    katsetus.Replace(";", ",")
                    .Substring(
                    katsetus.IndexOf("Temp"))
                    ;
                temp = temp.Substring(temp.IndexOf(":") + 1);
                temp = temp.Substring(0, temp.IndexOf(","));
                return temp;
            }
            catch { }
            return "";
        }
        
        static void Main(string[] args)
        {
            var ilmalogi = File.ReadAllLines(@"..\..\ilmalogi2.txt")
                .Select(x => x.Split(' '))
                .Where(x => x.Length == 4)
                .Select(x => new { Kuupäev = x[0], Kell = x[1], Temperatuur = x[3].Replace("=", ":").LeiaTemp() })
                .Where(x => x.Temperatuur != "")
                .Select(x => new { Aeg = DateTime.Parse(x.Kuupäev + " " + x.Kell), Temperatuur = double.Parse(x.Temperatuur, System.Globalization.CultureInfo.InvariantCulture)})
                .ToList()
                ;

            //foreach (var x in temperatuurid) Console.WriteLine(x);
            var päevad = ilmalogi.ToLookup(x => x.Aeg.Date);

            Console.WriteLine(
            päevad
                .Select(x => new { Kuupäev = x.Key, Max = x.Max(y => y.Temperatuur), Min = x.Min(y => y.Temperatuur) })
                .Select(x => new { x.Kuupäev, x.Max, x.Min, Vahe = x.Max - x.Min })
                .OrderByDescending(x => x.Vahe)
                .FirstOrDefault()
                );

            var nädalad = ilmalogi
                .Select(x => new { x.Aeg, x.Temperatuur, Nädal = new DateTime(1955, 03, 07).AddDays((x.Aeg - new DateTime(1955, 03, 07)).Days / 7 * 7) })
                .ToLookup(x => x.Nädal)
                ;
            //foreach (var x in nädalad) Console.WriteLine(x.Key.ToString("(ddd) dd.MMMM.yyyy"));
            Console.WriteLine(
                string.Join("\n",
                        nädalad
                            .Select(x => new { Kuupäev = x.Key, Max = x.Max(y => y.Temperatuur), Min = x.Min(y => y.Temperatuur) })
                            .Select(x => new { x.Kuupäev, x.Max, x.Min, Vahe = x.Max - x.Min })
                            .OrderBy(x => x.Vahe)
                            //.FirstOrDefault()
                )            
                );

            // kolme järjestikkuse päeva keskmine (näiteks)
            // päevad on meil muutujas päevad juba olemas
            var keskmised =
            päevad.Select(x => x.Key)
                .Select(x => new { päev = x, andmed = päevad[x].Union(päevad[x.AddDays(1)]).Union(päevad[x.AddDays(2)]) })
                .Select(x => new { x.päev, keskmine = x.andmed.Average(y => y.Temperatuur) });

            foreach(var x in keskmised)
                Console.WriteLine($"keskmine {x.päev.ToShortDateString()} kuni {x.päev.AddDays(2).ToShortDateString()} on {x.keskmine}");
                


                

        }


    }
}
