﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuutujaEluiga
{
    class Program
    {
        static void Main(string[] args)
        {

            Inimene i;
            i = new Inimene { Nimi = "Henn", Kaasa = new Inimene {Nimi = "Maris" } };
            Console.WriteLine(i);
            Console.WriteLine(i.Kaasa.Kaasa?.Nimi??"kaasat veel ei ole"); // ?. pöördu selle olematu asja nime poole




            var x = i?.Vanus;
            int? arv1 = 0; // -2 miljardit kuni +2 miljardit ja null
            int? arv2 = 0; // -2 miljardit kuni +2 miljardit ja null
            var tulemus =  arv1??0 +arv2??0;
            Nullable<int> arv3 = null;

            // . - member call operation
            // [] - index call operation
            // ?. - conditional member call
            // ?? - coalesce operatsioon

            // i?.Kaasa
            // i == null ? null : i.Kaasa
            // av ?? "väärtus"
            // av == null ? "väärtus" : av

            int[] arvud = new int[5];
            // ? arvud[3] // --> väärtus on 0
            Inimene[] rahvas = new Inimene[5];
            // ? rahvas[3] // --> väärtus on null
            Miski[] miskid = new Miski[5];
            // ? miskid[3] // --> väärtus on "default miski" {NImetus = "", Väärtus = 0}

            // int - vaikimisi 0
            // int? - vaikimisi null
            int?[] kahtlasedArvud = new int?[5];
            // kahtlasedArvud[3] // --> väärtus on null 

            

        }
    }

    class Inimene
    {
        public static int InimesteArv = 0;

        public Inimene() { InimesteArv++; }

        public string Nimi; // vaikimisi väärtuseks on ""
        public int Vanus; // vaikimisi e default väärtused - 0 (ümmargune)
        public Inimene Kaasa; // vaikimisi e default - null (kandiline)

        public int Sõrmi = 10;
        public int Varbaid = 10;
        


        public Miski Miski; // selle vaikimisi väärtus on vaikimisi struct

        public override string ToString() => $"{Nimi} vanusega {Vanus}";
    }

    struct Miski
    {
        public static int AsjadeArv ;

        public string Nimetus;
        public int Väärtus;
    }
    // structe (otse) kasutatakse väga harva
}
