﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KlassideTuletamiseYlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            // kui kasutada Array-d, siis tuleb sorteerida
            // kui kasutada SortedSet, siis on kohe serteeritud
            // kui kasutada Dictionary, siis ei ole sorteeritud
            // kui kasutada SortedList, siis on sorteeritud, aga tuleb võti välja mõelda
            // Array ja siis ArraySort oleks suure koguse puhul efektiivsem
            SortedSet<Inimene> koolipere = new SortedSet<Inimene>
            {
                //new Õpilane {Nimi = "Henn", Klass = "1A", Isikukood = "35503070211"},
                //new Õpilane {Nimi = "Ants", Klass = "1A", Isikukood = "35503070211"},
                //new Õpilane {Nimi = "Peeter", Klass = "1A", Isikukood = "35503070211"},
                //new Õpilane {Nimi = "Jaak", Klass = "1A", Isikukood = "35503070211"},
                //new Õpetaja {Nimi = "Malle", Klass = "1A", Aine = "maketmaatika"},
                //new Õpetaja {Nimi = "Tiit", Aine = "joonistamine"},
            };

            string õpilasteFail = @"..\..\õpilased.txt";
            string õpsideFail = @"..\..\õpetajad.txt";

            foreach (var rida in File.ReadAllLines(õpilasteFail))
            {
                var osad = rida.Replace(", ", ",").Split(',');
                koolipere.Add(new Õpilane
                {
                    Isikukood = osad[0],
                    Nimi = osad[1],
                    Klass = osad[2]
                });
            }
            foreach (var rida in File.ReadAllLines(õpsideFail))
            {
                var osad = (rida+",,").Replace(", ", ",").Split(',');
                koolipere.Add(new Õpetaja
                {
                    Isikukood = osad[0],
                    Nimi = osad[1],
                    Klass = osad[3],
                    Aine = osad[2]
                });
            }


            foreach (var x in koolipere) Console.WriteLine(x);
        }
    }

    class Inimene : IComparable
    {
        public string Nimi; // { get; set; }
        public string Isikukood { get; set; }


        // seda asja peaks üle rääkima
        // klassil ütleme, et ta on IComparable
        // implementeerime CompareTo funktsiooni
        // teisendame selle nimede CompareTo funktsiooniks
        public int CompareTo(object obj) => Nimi.CompareTo((obj as Inimene)?.Nimi ?? "");

        // siiani
        public override string ToString() => $"{Nimi} (IK: {Isikukood})";
    }

    class Õpetaja : Inimene
    {
        public string Aine { get; set; }
        public string Klass { get; set; } = ""; // klass, mida juhatab - kui "" siis ei juhata

        public override string ToString() => 
            $"{Aine} õpetaja " +
            base.ToString()
            + (Klass == "" ? "" : $"({Klass} klassi juhataja)")
            ;

    }

    class Õpilane : Inimene
    {
        public string Klass { get; set; } // klass, kus õpib

        public override string ToString() => $"{Klass} klassi õpilane " + base.ToString(); 


    }
}
