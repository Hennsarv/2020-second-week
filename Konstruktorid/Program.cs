﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktorid
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene("35503070211") { Nimi = "Henn", Vanus = 64 };
            Inimene ants = new Inimene("38001010211") { Nimi = "Ants", Vanus = 40 };
            Inimene peeter = new Inimene("39207070211") { Nimi = "Peeter", Vanus = 28 };
            Inimene jumbu = new Inimene { Nimi = "Jumbu" };
            new Inimene("Joosep", "50101010000"); // see kolmas konstruktor
            new Inimene("Kalle", "37707070000", 43);


            Console.WriteLine(henn.Isikukood);
            Console.WriteLine(henn);

            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);

        }
    }

    class Inimene
    {
        public static int InimesteArv { get; private set; } = 0; // see on static property 
        // seda saab muuta ainult klassi siseselt

        static List<Inimene> _Inimesed = new List<Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.AsEnumerable(); 
 
        //public static int Miski { get; set; }

        // objekti väljad (instantsi e eksemplari omad)
        public readonly int Nr = ++InimesteArv; // readonly välja saab ainult objekti loomisel väärtustada
        public string Nimi;
        public int Vanus;

        public readonly string Isikukood = "00000000000";

        // see siin on konstruktor
        public Inimene(string isikukood) 
        {
            
            Isikukood = isikukood;
            _Inimesed.Add(this);
        }

        public Inimene() : this("00000000000") { }
        public Inimene(string nimi, string isikikood) : this(isikikood) => Nimi = nimi; 
        public Inimene(string nimi, string isikukood, int vanus) : this (isikukood) 
        => (Nimi, Vanus) = (nimi, vanus);
        //{
        //    Nimi = nimi;
        //    Vanus = vanus;
        //}

        // see on vajalik, et inimest saaks stringiks teisendada (ConsoleWriteLine, $-string jt)
        // sellise rea võiks igale klassile (structile) lisada
        // kuidu teeks ToString() tast "Konstruktorid.Inimene"
        public override string ToString() 
            => $"{Nr}. {Nimi} vanus: {Vanus} " +
            $"(kokku {InimesteArv} {(InimesteArv==1?"inimene":"inimest")})";
    }
}
