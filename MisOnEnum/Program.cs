﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnEnum
{

    enum Mast { Risti, Ruutu, Ärtu, Poti, Pada = 3 }
    enum Kaart { Kaks, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss}
    enum Sugu { Naine, Mees, Tundmatu = 99 }

    [Flags] enum Tunnus { Suur = 1, Puust = 2, Punane = 4, Kolisev = 8 }

    struct Mängukaart
    {
        public Mast Mast;
        public Kaart Kaart;

    }

    class Program
    {
        static void Main(string[] args)
        {
            Mast m = Mast.Poti;
            m++;
            Console.WriteLine(m);

            int a = 15;
            Tunnus t = (Tunnus)a;
            Console.WriteLine(t);

            t = Tunnus.Kolisev | Tunnus.Punane;
            if ((t & Tunnus.Punane) == Tunnus.Punane) Console.WriteLine("on punane");
            t |= Tunnus.Puust; // lisame tunnuse Puust
            Console.WriteLine(t);
            t ^= Tunnus.Punane; // eemaldame tunnuse Punane
            Console.WriteLine(t);

            Random r = new Random();

            int juhuslik = r.Next(52);

            Console.WriteLine($"{(Mast)(juhuslik%4)} {(Kaart)(juhuslik/4)}");




        }
    }
}
