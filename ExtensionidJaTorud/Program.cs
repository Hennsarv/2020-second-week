﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExtensionidJaTorud
{
    static class E
    {

        public static IEnumerable<string> LoeRead(this string filename)
        {
            return File.ReadAllLines(filename);
        }

        // Listil on ForEach, aga kahjuks teistel kollektsioonidel ei ole
        // siin on hea vaadata, et Func<T,T> kõrval on olemas ka Action<T>
        // kui Func on funktsioon (x => avaldis(x))
        // siis Action on void (meetod) ( x => miskitegevus)
        public static void MyForEach<T>(this IEnumerable<T> mass, Action<T> a)
        {
            foreach (var x in mass) a(x);
        }

        public static Paar Korruta(this Paar t1, Paar t2) => new Paar(t1.a * t2.a, t1.b * t2.b);
        // funktsioon on staatiline

        public static string ToProper(this string s)
        {
            return s == "" ? "" :
                s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();
        }

        public static string Join<T>(this IEnumerable<T> mass, string sep)
            => "{"+ string.Join(sep, mass) + "}";

        public static IEnumerable<int> Paaris(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 == 0) yield return x;
        }
        public static IEnumerable<int> Paaritu(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 != 0) yield return x;
        }

        public static IEnumerable<T> Millised<T>(this IEnumerable<T> mass, Func<T,bool> f)
        {
            foreach (var x in mass)
                if (f(x)) yield return x; 
        }

        public static IEnumerable<T> Võta<T>(this IEnumerable<T> mass, int mitu) // Take
        {
            foreach (var x in mass)
                if (mitu-- > 0) yield return x;
        }

        public static IEnumerable<T> Jäta<T>(this IEnumerable<T> mass, int mitu) // Skip
        {
            foreach (var x in mass)
                if (mitu-- <= 0) yield return x;
        }




    }

    static class Program // klass on staatiline
    {
 
        static void Main(string[] args)
        {
            Console.WriteLine("Anna oma nimi: ");
            Console.WriteLine(new string(Console.ReadLine().ToProper().Reverse().ToArray()));

            int[] arvud = { 1, 2, 7, 3, 4, 2, 9, 6, 2, 11, 14, 23, 27, 28, 42 };
            //            foreach (var x in arvud) Console.WriteLine(x);
            //Console.WriteLine(string.Join(",", arvud));

            Console.WriteLine(
                arvud
                //.Paaritu()
                .Where(x => x % 2 == 0)
                .Skip(3)
                .Join(",")
                );

            Console.WriteLine(
                arvud
                .Where(x => x > 5)
                .Take(5)
                .Join(",")
                
                );

            Console.WriteLine(
                arvud
                .Select(x => x*x)
                .Join(",")

                );

            var spp = @"..\..\spordipäeva protokoll.txt"
                .LoeRead()
                .Skip(1)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Where(x => x.Length > 2)
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                .ToArray();
            var spp2 = spp
                //.OrderBy(x => x.Distants / x.Aeg)
                //.Reverse()
                .OrderByDescending(x => x.Distants * 1.0 / x.Aeg)
                .ToLookup(x => x.Nimi)
                .Select(x => new { Nimi = x.Key, Keskmine = x.DefaultIfEmpty().Average(y => y?.Distants??0 / y?.Aeg??1)})
                ;
            Console.WriteLine(spp2.Join(",\n"));

            var q1 = from x in spp
                    where x.Distants == 100
                    orderby x.Nimi
                    select x.Nimi;
            Console.WriteLine(" 100 meetrit jooksid" + q1.Join(", "));

            var q2 = spp.Where(x => x.Distants == 100).OrderBy(x => x.Nimi).Select(x => x.Nimi);

            var distantsiKiireimad =
                spp
                .ToLookup(x => x.Distants)
                .Select(x => new { Distants = x.Key, Kiireim = x.OrderByDescending(y => y.Distants * 1.0 / y.Aeg).First().Nimi });

            Console.WriteLine("Distantside kiireimad olid:");
            distantsiKiireimad
                /*          // switching comment - / selle rea algusse vahetab aktiivse osa koodist
                .ToList()   // teen ta listiks, kuna listil on tore asi ForEach
                            //teistel ei ole, aga ma kohe teen - MyForEach
                .ForEach(x => Console.WriteLine(x));
                // */ .MyForEach(x => Console.WriteLine(x));


            #region selgitus mis on lambda funktsioon
            //Func<int, int> ruut = x => { return x * x; };
            //Func<int, int> kuup = x => x * x * x;
            //Func<int, int, int> liida = (x, y) => x + y;

            //Console.WriteLine(ruut(4));
            //Console.WriteLine(kuup(5)); 
            #endregion



            #region paaride näide
            //Paar t = new Paar(3, 4);
            //Console.WriteLine(t);
            //Paar teine = new Paar(5, 3);
            //Console.WriteLine(t.Liida(teine));
            //Console.WriteLine(Paar.Liida(t,teine));

            //Console.WriteLine(Korruta(t,teine));
            //Console.WriteLine(t.Korruta(teine)); 
            #endregion
        }
    }

    class Paar
    {
        public int a;
        public int b;
        public Paar(int a, int b) => (this.a, this.b) = (a,b);

        public override string ToString() => 
                                        // (a, b).ToString(); 
                                         $"({a},{b})";

        public Paar Liida(Paar t) => new Paar(this.a + t.a, this.b + t.b);

        public static Paar Liida(Paar t1, Paar t2) => new Paar(t1.a + t2.a, t1.b + t2.b);

    }

}
