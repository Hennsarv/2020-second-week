﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Inimene henn =  new Inimene();
            henn.Eesnimi = "Henn";
            henn.Perenimi = "Sarv";
            henn.Vanus = 64;
            Console.WriteLine(henn);
            Inimene teine = new Inimene
            {
                Eesnimi = "Peeter",
                Perenimi = "Suur",
                Vanus = 28
            };

            Inimene kolmas = new Inimene { Eesnimi = "Ants", Vanus = 40, Perenimi = "Saunamees" };



            Console.WriteLine(henn);
            Console.WriteLine(teine);
            Console.WriteLine(kolmas);

            Console.WriteLine($"Meil on inimesi {Inimene.InimesteArv()}");
            Console.WriteLine($"Meie tulevik on {Inimene.Tulevik}");

            Console.WriteLine(henn.Nimi());
        }
    }

   
        

}
