﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    partial class Inimene
    {
        // staatilisi asju on kamba peale 1
        private static int _InimesteArv = 0; // lisasin staatilise välja
        static string _Tulevik = "Helge";

        // klassi funktsioon (näeb ainult staatilisi asju)
        public static int InimesteArv()
        { 
            return _InimesteArv; 
        }
        public static string Tulevik => _Tulevik; // mis asi see on, me ei tea

        // objekti välju on igals eksemplaril oma
        public int Number = ++_InimesteArv; // lisasin ühe "mittestaatilise" välja
        public string Eesnimi;
        public string Perenimi;
        public int Vanus;

        public string Nimi() // instantsi funktsioon (olemas müstiline this)
        {
            return $"{this.Eesnimi} {this.Perenimi}";
        }

        // pisut kohendasin seda tostringi
        public override string ToString() => $"{this.Number}. ({_InimesteArv}-st) {this.Nimi()} vanusega {Vanus}";



    }
}
