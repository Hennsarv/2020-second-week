﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace SpordipäevJsoniks
{
    class Program
    {
        static void Main(string[] args)
        {
            // nii on mugav teha, ehk tahan teinekord mõne teise failiga sama teha
            string folder = @"..\..\";              // see on kausta nimi, kus failid
            string name = "spordipäeva protokoll";  // see faili "nimi"
            string filenameTxt = $"{folder}{name}.txt";     // failinimi laiendiga txt
            string filenameJson = $"{folder}{name}.json";   // failinimi laiendiga json

            Console.Write("Kas teisendan või kontrollin (t, k): "); // selle lisasin hiljem

            switch(Console.ReadLine())
            {
                case "t":

                // kõigepealt loeme faili mällu
                var loetudRead = File.ReadAllLines(filenameTxt); // nüüd on mälus massiivina
                List<ProtokolliRida> protokolliRead = new List<ProtokolliRida>(); // siia lisame
                    for (int i = 1; i < loetudRead.Length; i++) // esimesest (0-rida vahele)
                    {
                        var rida = loetudRead[i]
                            .Replace(", ", ",")  // korjame koma tagant tühikud
                            .Split(','); // ja tükeldame
                        if (rida.Length > 2) // igaks juhuks kontrollime, jätame tühjad read vahele
                        {
                            protokolliRead.Add(
                          new ProtokolliRida
                          {
                              Nimi = rida[0],
                              Distants = int.Parse(rida[1]),
                              Aeg = int.Parse(rida[2])
                          }
                          );
                        }
                    }
                // selle kohapea-l on read loetud mällu ja tehtud neist List
                File.WriteAllText(
                        filenameJson, // siin on failinimi
                        JsonConvert.SerializeObject(protokolliRead) // ja siin on teisendatud Json-string
                    );
                    break;
                // lisan juurde (täitsa iseseisvalt tulin selle peale)
                // bloki salvestatud jsoni kontrollimiseks
                case "k":
                    List<ProtokolliRida> loetud =
                        JsonConvert
                        .DeserializeObject<List<ProtokolliRida>>(File.ReadAllText(filenameJson));
                    // loeme kontrolliks faili mällu ja teisendame listiks
                    foreach(var x in loetud)
                    {
                        Console.WriteLine($"{x.Nimi} distants: {x.Distants} kiirus: {x.Kiirus:F2}");
                    }
                    break;
            }
        }
    }

    // teen omale klassi (NB - public)
    public class ProtokolliRida
    {
        public string Nimi { get; set; }
        public int Distants { get; set; }
        public int Aeg { get; set; }
        public double Kiirus => Aeg == 0 ? 0 : Distants  * 1.0 / Aeg;
    }
}
