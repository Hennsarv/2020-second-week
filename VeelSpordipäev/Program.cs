﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace VeelSpordipäev
{
    class Program
    {
        static void Main(string[] args)
        {
            var protokoll = File.ReadAllLines(@"..\..\spordipäeva protokoll.txt")
                .Skip(1)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg})
                .ToList()
                ;
            var jooksjad = protokoll.ToLookup(x => x.Nimi);

            // kõige kiirema leidmine
            Console.WriteLine("\nKõige kiirem jooksja\n");
            Console.WriteLine(
                protokoll
                .Where(x => x.Distants == 2000)
                .OrderByDescending(x => x.Kiirus).FirstOrDefault()?.Nimi??"puudub"
                );
            // kui on võimalus, et sama kiireid on mitu, siis on veel selline variant
            Console.WriteLine(
                string.Join("\n",
                
                protokoll
                .GroupBy(x => x.Kiirus).OrderBy(x => x.Key).LastOrDefault()
                )                
                );
            // kes on kõige stabiilseim
            Console.WriteLine(
                //string.Join("\n",
                jooksjad
                    .Where(x => x.Count() > 1)
                    .Select(x => new { Nimi = x.Key, Min = x.Min(y => y.Kiirus), Max = x.Max(y => y.Kiirus) })
                    .Select(x => new { x.Nimi, Vahe = x.Max - x.Min, x.Max, x.Min })
                    .OrderBy(x => x.Vahe)
                    .FirstOrDefault()
                    //)
                ); 

        }
    }
}
